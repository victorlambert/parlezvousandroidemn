package fr.emn.fil.parlezvousandroid.util;

import android.os.Looper;

import fr.emn.fil.parlezvousandroid.BuildConfig;

/**
 * Created by victor on 06/02/2014.
 */
public class ThreadPreconditions {
    public static void checkOnMainThread() {
        if (BuildConfig.DEBUG) {
            if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
                throw new IllegalStateException("This method should be called from the Main Thread");
            }
        }
    }
}
