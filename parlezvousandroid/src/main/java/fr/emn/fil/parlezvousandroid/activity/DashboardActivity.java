package fr.emn.fil.parlezvousandroid.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.util.List;

import fr.emn.fil.parlezvousandroid.R;
import fr.emn.fil.parlezvousandroid.model.Message;
import fr.emn.fil.parlezvousandroid.util.Constants;
import fr.emn.fil.parlezvousandroid.util.Utils;


public class DashboardActivity extends Activity {

    private Button button_list_messages;
    private Button button_send_message;
    private String username;
    private String password;
    private Intent intent_send_message;
    private Intent intent_list_message;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fullscreen Activity
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_dashboard);

        SharedPreferences prefs = getSharedPreferences(Constants.NAME_CREDENTIALS_PREFS, MODE_PRIVATE);
        username = prefs.getString(Constants.USERNAME_PREFS, "");
        password = prefs.getString(Constants.PASSWORD_PREFS, "");

        button_list_messages = (Button) findViewById(R.id.list_messages);
        button_send_message = (Button) findViewById(R.id.send_message);

        button_list_messages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent_list_message = new Intent(DashboardActivity.this, MessageListActivity.class);
                startActivity(intent_list_message);
            }
        });

        button_send_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent_send_message = new Intent(DashboardActivity.this, SendMessageActivity.class);
                startActivity(intent_send_message);
            }
        });

    }



}