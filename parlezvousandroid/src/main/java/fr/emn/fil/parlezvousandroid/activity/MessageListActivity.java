package fr.emn.fil.parlezvousandroid.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.emn.fil.parlezvousandroid.R;
import fr.emn.fil.parlezvousandroid.adapter.MessageAdapter;
import fr.emn.fil.parlezvousandroid.model.Message;
import fr.emn.fil.parlezvousandroid.util.Constants;
import fr.emn.fil.parlezvousandroid.util.Utils;

public class MessageListActivity extends ActionBarActivity {

    private MessageAdapter adapter;
    private ListMessageTask list_message_task;
    private String username;
    private String password;
    private ListView message_listview;
    private ImageButton refresh_button;
    private ProgressBar list_message_progressbar;
    private EditText txtbox_msg;
    private ImageButton btn_send_msg;
    private SendMsgTask sendMsgTask;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fullscreen Activity
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_message_list);

        SharedPreferences prefs = getSharedPreferences(Constants.NAME_CREDENTIALS_PREFS, MODE_PRIVATE);
        username = prefs.getString(Constants.USERNAME_PREFS, "");
        password = prefs.getString(Constants.PASSWORD_PREFS, "");

        message_listview = (ListView) findViewById(R.id.listview);
        refresh_button = (ImageButton) findViewById(R.id.refresh);
        list_message_progressbar = (ProgressBar) findViewById(R.id.progressbar_list_message);
        txtbox_msg = (EditText) findViewById(R.id.txtbox_message);
        btn_send_msg = (ImageButton) findViewById(R.id.send_message);

        adapter = new MessageAdapter(this);
        message_listview.setAdapter(adapter);
        message_listview.setDivider(null);


        refreshMessageList();


        refresh_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshMessageList();
            }
        });
        btn_send_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = txtbox_msg.getText().toString();
                if (message.trim().equalsIgnoreCase("")) {
                    Toast.makeText(MessageListActivity.this, "Veuillez taper votre message !", Toast.LENGTH_SHORT).show();
                } else {
                    if(sendMsgTask == null || sendMsgTask.getStatus() == AsyncTask.Status.FINISHED){
                        sendMsgTask = new SendMsgTask();
                    }
                    if(sendMsgTask.getStatus() == AsyncTask.Status.PENDING){
                        sendMsgTask.execute(username,password,message);
                    }
                }
            }
        });

    }


    private void refreshMessageList(){
        if(list_message_task == null || list_message_task.getStatus() == AsyncTask.Status.FINISHED){
            list_message_task = new ListMessageTask();
        }
        if(list_message_task.getStatus() == AsyncTask.Status.PENDING){
            list_message_task.execute();
        }
    }


    private class ListMessageTask extends AsyncTask<String, String, List> {

        @Override
        protected void onPreExecute() {
            list_message_progressbar.setVisibility(View.VISIBLE);
            refresh_button.setEnabled(false);
            btn_send_msg.setEnabled(false);
        }

        @Override
        protected List<Message> doInBackground(String... params) {

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet("http://" + Constants.HOST + "/messages/" + username + "/" + password);

            String content = null;
            try {
                HttpResponse response = client.execute(request);
                content = Utils.convertInputStreamToString(response.getEntity().getContent());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return Utils.convertStringToMessageList(content);
        }

        @Override
        protected void onPostExecute(List list) {
            super.onPostExecute(list);
            adapter.updateMessages(list);
            list_message_progressbar.setVisibility(View.INVISIBLE);
            message_listview.setSelection(adapter.getCount() - 1);
            refresh_button.setEnabled(true);
            btn_send_msg.setEnabled(true);
        }
    }



    private class SendMsgTask extends AsyncTask<String, String, Boolean> {

        @Override
        protected void onPreExecute() {
            list_message_progressbar.setVisibility(View.VISIBLE);
            refresh_button.setEnabled(false);
            btn_send_msg.setEnabled(false);        }

        @Override
        protected Boolean doInBackground(String... params) {
            String username = params[0];
            String password = params[1];
            String message = params[2];

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet("http://" + Constants.HOST + "/message/" + username + "/" + password + "/" +Uri.encode(message));

            String content = null;
            try {
                HttpResponse response = client.execute(request);
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            list_message_progressbar.setVisibility(View.INVISIBLE);
            txtbox_msg.setText("");
            refresh_button.setEnabled(true);
            btn_send_msg.setEnabled(true);
            Toast.makeText(MessageListActivity.this, "Message envoyé !", Toast.LENGTH_SHORT).show();
            refreshMessageList();
        }

    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.message_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
