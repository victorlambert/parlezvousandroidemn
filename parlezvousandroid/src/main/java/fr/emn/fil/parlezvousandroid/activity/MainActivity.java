package fr.emn.fil.parlezvousandroid.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.view.View.OnClickListener;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

import fr.emn.fil.parlezvousandroid.R;
import fr.emn.fil.parlezvousandroid.util.Constants;
import fr.emn.fil.parlezvousandroid.util.Utils;


public class MainActivity extends ActionBarActivity {

    private final String TAG = MainActivity.class.getName();

    private Button clean_button;
    private Button submit_button;
    private EditText username_edit_text;
    private EditText password_edit_text;
    private ProgressBar loading_progress_bar;
    private TextView error_text_view;
    private LoginTask login_task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fullscreen Activity
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        clean_button = (Button) findViewById(R.id.clean_button);
        submit_button = (Button) findViewById(R.id.submit_button);
        username_edit_text = (EditText) findViewById(R.id.username_field);
        password_edit_text = (EditText) findViewById(R.id.password_field);
        loading_progress_bar = (ProgressBar) findViewById(R.id.loading);
        error_text_view = (TextView) findViewById(R.id.error_message);

        clean_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                username_edit_text.setText("");
                password_edit_text.setText("");
            }
        });

        submit_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (username_edit_text.getText().toString().trim().equalsIgnoreCase("")
                        || password_edit_text.getText().toString().trim().equalsIgnoreCase("")) {
                    error_text_view.setVisibility(View.VISIBLE);
                } else {
                    if(login_task == null || login_task.getStatus() == Status.FINISHED){
                        login_task = new LoginTask();
                    }
                    if(login_task.getStatus() == Status.PENDING){
                        String username = username_edit_text.getText().toString();
                        String password = password_edit_text.getText().toString();
                        login_task.execute(username,password);
                    }
                }

            }
        });

        SharedPreferences prefs = getSharedPreferences(Constants.NAME_CREDENTIALS_PREFS, MODE_PRIVATE);
        username_edit_text.setText(prefs.getString(Constants.USERNAME_PREFS, ""));
        password_edit_text.setText(prefs.getString(Constants.PASSWORD_PREFS, ""));


    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause!");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "onRestart!");
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i(TAG, "onRestoreInstanceState!");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume!");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("isErrorMessageVisible", error_text_view.getVisibility() == View.VISIBLE);
        Log.i(TAG, "onSaveInstanceState!");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy!");
    }




    private class LoginTask extends AsyncTask<String, String, Boolean> {


        @Override
        protected void onPreExecute() {
            loading_progress_bar.setVisibility(View.VISIBLE);
            submit_button.setEnabled(false);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            String username = params[0];
            String password = params[1];

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet("http://" + Constants.HOST + "/connect/" + username + "/" + password);

            String content = null;
            try {
                HttpResponse response = client.execute(request);
                content = Utils.convertInputStreamToString(response.getEntity().getContent());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return Boolean.valueOf(content);
        }

        @Override
        protected void onPostExecute(Boolean login) {
            loading_progress_bar.setVisibility(View.INVISIBLE);
            submit_button.setEnabled(true);

            String msg;
            if(login){
                msg = "Vous êtes maintenant connecté!";
                SharedPreferences prefs = getSharedPreferences("user_credentials", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(Constants.USERNAME_PREFS, username_edit_text.getText().toString());
                editor.putString(Constants.PASSWORD_PREFS, password_edit_text.getText().toString());
                editor.commit();
                Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
                startActivity(intent);
            }else{
                msg = "Vous n'êtes pas connecté!";
            }
            Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
        }

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}

