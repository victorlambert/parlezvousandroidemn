package fr.emn.fil.parlezvousandroid.util;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import fr.emn.fil.parlezvousandroid.model.Message;

/**
 * Created by victor on 06/02/2014.
 */
public class Utils {

    public static String convertInputStreamToString(InputStream is) {
        String line = "";
        StringBuilder builder = new StringBuilder();

        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        try {
            while ((line = rd.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }

    public static List<Message> convertStringToMessageList(String message_list_string){

        if(message_list_string ==null){
            return null;
        }else{

            String[] tab_messages = message_list_string.split(";");
            List<Message> list_messages = new ArrayList<Message>(tab_messages.length);
            for(String msg : tab_messages){
                if(!msg.trim().equalsIgnoreCase("") && msg.contains(":")){
                    String[] sequence = msg.split(":");
                    String author = sequence[0];
                    String content = "";
                    if(sequence.length > 1){
                        content = sequence[1];
                    }
                    list_messages.add(new Message(author,content));
                }
            }

            return list_messages;
        }
    }
}
