package fr.emn.fil.parlezvousandroid.activity;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

import fr.emn.fil.parlezvousandroid.R;
import fr.emn.fil.parlezvousandroid.util.Constants;

public class SendMessageActivity extends ActionBarActivity {

    TextView header_send_msg;
    Button btn_send_msg;
    EditText txtbox_msg;
    ProgressBar loading_progress_bar;
    SendMsgTask sendMsgTask;
    String username;
    String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fullscreen Activity
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_send_message);

        header_send_msg = (TextView) findViewById(R.id.header_send_message);
        btn_send_msg = (Button) findViewById(R.id.btn_send_message);
        txtbox_msg = (EditText) findViewById(R.id.txtbox_message);
        loading_progress_bar = (ProgressBar) findViewById(R.id.progressbar_send_message);

        SharedPreferences prefs = getSharedPreferences(Constants.NAME_CREDENTIALS_PREFS, MODE_PRIVATE);
        username = prefs.getString(Constants.USERNAME_PREFS, "");
        password = prefs.getString(Constants.PASSWORD_PREFS, "");

        btn_send_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = txtbox_msg.getText().toString();
                if (message.trim().equalsIgnoreCase("")) {
                    Toast.makeText(SendMessageActivity.this, "Veuillez taper votre message !", Toast.LENGTH_SHORT).show();
                } else {
                    if(sendMsgTask == null || sendMsgTask.getStatus() == AsyncTask.Status.FINISHED){
                        sendMsgTask = new SendMsgTask();
                    }
                    if(sendMsgTask.getStatus() == AsyncTask.Status.PENDING){
                        sendMsgTask.execute(username,password,message);
                    }
                }

            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.send_message, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class SendMsgTask extends AsyncTask<String, String, Boolean> {


        @Override
        protected void onPreExecute() {
            loading_progress_bar.setVisibility(View.VISIBLE);
            btn_send_msg.setEnabled(false);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            String username = params[0];
            String password = params[1];
            String message = params[2];

            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet("http://" + Constants.HOST + "/message/" + username + "/" + password + "/" + Uri.encode(message));

            String content = null;
            try {
                HttpResponse response = client.execute(request);
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean login) {
            loading_progress_bar.setVisibility(View.INVISIBLE);
            txtbox_msg.setText("");
            btn_send_msg.setEnabled(true);
            Toast.makeText(SendMessageActivity.this, "Message envoyé !", Toast.LENGTH_SHORT).show();
        }

    }

}
