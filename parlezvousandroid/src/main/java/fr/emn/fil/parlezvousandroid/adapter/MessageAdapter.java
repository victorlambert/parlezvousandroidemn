package fr.emn.fil.parlezvousandroid.adapter;

import android.app.ActionBar;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import fr.emn.fil.parlezvousandroid.R;
import fr.emn.fil.parlezvousandroid.model.Message;
import fr.emn.fil.parlezvousandroid.util.Constants;
import fr.emn.fil.parlezvousandroid.util.ThreadPreconditions;

/**
 * Created by victor on 06/02/2014.
 */

public class MessageAdapter extends BaseAdapter {

    private List<Message> messages = Collections.emptyList();
    private String username;

    private final Context context;

    public MessageAdapter(Context context) {
        this.context = context;
    }

    public void updateMessages(List<Message> messages) {
        ThreadPreconditions.checkOnMainThread();
        this.messages = messages;
        notifyDataSetChanged();

    }

    @Override
    public int getCount() {
        return messages.size();
    }


    @Override
    public Message getItem(int position) {
        return messages.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView author_text_view;
        TextView message_text_view;
        LinearLayout layout;

        Message message = getItem(position);

        SharedPreferences prefs = context.getSharedPreferences(Constants.NAME_CREDENTIALS_PREFS, context.MODE_PRIVATE);
        username = prefs.getString(Constants.USERNAME_PREFS, "");
        boolean ismymsg = username.equalsIgnoreCase(message.getAuthor());
        ViewHolder viewHolder;
        if (convertView == null)
        {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.message_row, parent, false);
            author_text_view = (TextView) convertView.findViewById(R.id.author);
            message_text_view = (TextView) convertView.findViewById(R.id.message);
            layout = (LinearLayout) convertView.findViewById(R.id.layout_msg);
            viewHolder = new ViewHolder(author_text_view, message_text_view, layout);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
            author_text_view = viewHolder.author_text_view;
            message_text_view = viewHolder.message_text_view;
            layout = viewHolder.layout_msg;
        }

        author_text_view.setText(message.getAuthor());
        message_text_view.setText(message.getMessage());
        if(ismymsg){
            author_text_view.setText("");
            author_text_view.setBackgroundColor(Color.TRANSPARENT);
            layout.setBackgroundResource(R.drawable.rounded_corners);
        }else{
            layout.setBackgroundResource(R.drawable.rounded_corners_other);
        }

        return convertView;
    }

    private static class ViewHolder {
        public final TextView author_text_view;
        public final TextView message_text_view;
        public final LinearLayout layout_msg;

        public ViewHolder(TextView author_view, TextView message_view, LinearLayout layout) {
            this.author_text_view = author_view;
            this.message_text_view = message_view;
            this.layout_msg = layout;
        }
    }

}
