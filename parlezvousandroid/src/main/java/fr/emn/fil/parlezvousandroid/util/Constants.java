package fr.emn.fil.parlezvousandroid.util;

/**
 * Created by victor on 06/02/2014.
 */
public class Constants {
    public final static String HOST = "parlezvous.herokuapp.com";
    public final static String USERNAME_PREFS = "username";
    public final static String PASSWORD_PREFS = "password";
    public final static String NAME_CREDENTIALS_PREFS = "user_credentials";
}
